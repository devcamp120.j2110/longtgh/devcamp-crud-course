// Import thư viện mongoose JS
const mongoose = require('mongoose');

// Import Course Model
const { CourseModel } = require('../models/CourseModel');
// Import Review Model
const { ReviewModel } = require('../models/ReviewModel');

// Create Review
function createReview(req, res) {
     // Khởi tạo một đối tượng ReviewModel  mới truyền các tham số tương ứng từ request body vào
    const review = new ReviewModel({
        // Thuộc tính _id - Random một ObjectID
        _id: mongoose.Types.ObjectId(),
        // Thuộc tính stars - thuộc tính title từ request body
        stars: req.body.stars,
        // Thuộc tính note - thuộc tính title từ request body
        note: req.body.note,
    });

    // Gọi hàm review save - là 1 promise (Tiến trình bất đồng bộ)
    review.save()
        // Sau khi tạo review thành công
        .then(function(newReview) {
            // Lấy courseId từ params URL (Khác với Query URL (sau ?))
            var courseId = req.params.courseId;

            // Gọi hàm CourseModel .findOneAndUpdate truyền vào điều kiện (_id = ?) và thêm _id của review mới tạo vào mảng reviews
            return CourseModel.findOneAndUpdate({ _id: courseId }, {$push: {reviews: newReview._id}}, { new: true });
        })
        // Sau khi update course thành công trả ra status 200 - Success
        .then((updatedCourse) => {
            return res.status(200).json({
                success: true,
                message: 'New Review created successfully on Course',
                Course: updatedCourse,
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((error) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

// Get all review of course 
function getAllReviewOfCourse(req, res) {
    // Lấy courseId từ params URL (Khác với Query URL (sau ?))
    const courseId = req.params.courseId;

    // Gọi hàm .findById để tìm kiếm course dựa vào ID
    CourseModel.findById(courseId)
        // Lấy chi tiết Review dựa vào ánh xạ _id của từng phần tử trong trường reviews
        .populate({path: 'reviews'})
        // Nếu thành công trả ra status 200 - Success
        .then((singleCourse) => {
            res.status(200).json({
                success: true,
                message: `More reviews on ${singleCourse.title}`,
                Reviews: singleCourse
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This course does not exist',
                error: err.message,
            });
        });
}

// Get all review
function getAllReview(req, res) {
    ReviewModel.find()
        .select('_id stars note')
        .then((allReview) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all review',
                Review: allReview,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// Get single review
function getSingleReview(req, res) {
    const id = req.params.reviewId;

    ReviewModel.findById(id)
        .then((singleReview) => {
            res.status(200).json({
                success: true,
                message: `Get data on Review`,
                Review: singleReview,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This review does not exist',
                error: err.message,
            });
        });
}

// update review
function updateReview(req, res) {
    const reviewId = req.params.reviewId;
    const updateObject = req.body;
    ReviewModel.findByIdAndUpdate(reviewId, updateObject)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Review is updated',
                updateReview: updateObject,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
}

// delete a review
function deleteReview(req, res) {
    const id = req.params.reviewId;

    ReviewModel.findByIdAndRemove(id)
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}

module.exports = { createReview, getAllReviewOfCourse, getAllReview, updateReview, getSingleReview, deleteReview }
