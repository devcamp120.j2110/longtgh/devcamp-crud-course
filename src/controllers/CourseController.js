const mongoose = require("mongoose");

const { CourseModel } = require("../models/CourseModel");

function createCourse(request, response) {
    const course = new CourseModel({
        _id: mongoose.Types.ObjectId(),
        title: request.body.title,
        description: request.body.description,
        noStudent: request.body.noStudent
    });

    course.save()
        .then((newCourse) => {
            return response.status(200).json({
                message: "Success",
                course: newCourse
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function getAllCourse(request, response) {
    CourseModel.find()
        .select("_id title noStudent")
        .then((courseList) => {
            return response.status(200).json({
                message: "Success",
                courses: courseList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function getCourseDetail(request, response) {
    // Lấy courseId từ params URL
    const courseId = request.params.courseId;

    // Kiểm tra xem courseId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        CourseModel.findById(courseId)
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        course: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "CourseID is not valid"
        })
    }
}

function updateCourseByID(request, response) {
    // Lấy courseId từ params URL
    const courseId = request.params.courseId;

    const updateObject = request.body;

    // Kiểm tra xem courseId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        CourseModel.findByIdAndUpdate(courseId, updateObject)
            .then(() => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updateObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "CourseID is not valid"
        })
    }
}

function deleteCourse(request, response) {
    // Lấy courseId từ params URL
    const courseId = request.params.courseId;

    // Kiểm tra xem courseId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        CourseModel.findByIdAndDelete(courseId)
            .then(() => {
                return response.status(204).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "CourseID is not valid"
        })
    }
}

module.exports = { createCourse, getAllCourse, getCourseDetail, updateCourseByID, deleteCourse }
