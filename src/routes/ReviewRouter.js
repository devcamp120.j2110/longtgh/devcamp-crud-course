// Import thư viện Express
const express = require('express');

// Import các hàm của lớp controller
const { getAllReview, deleteReview, getSingleReview, updateReview } = require("../controllers/ReviewController");

// Khai báo exporess router
const router = express.Router();

// Khai báo các router dạng "/reviews" + url bên dưới sẽ gọi đến các hàm tương ứng

// /reviews - Get All Reviews
router.get('/', getAllReview);
// /reviews/:reviewId - Get Review By ID
router.get('/:reviewId', getSingleReview);
// /reviews/:reviewId - Update Review By ID
router.put('/:reviewId', updateReview);
// /reviews/:reviewId - Delete Review By ID
router.delete('/:reviewId', deleteReview);

// Export router dưới dạng module để sử dụng
module.exports = router;
