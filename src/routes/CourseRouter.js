const express = require("express");

const router = express.Router();

const { createCourse, getAllCourse, getCourseDetail, updateCourseByID, deleteCourse } = require("../controllers/CourseController");
const { createReview, getAllReviewOfCourse } = require("../controllers/ReviewController");

router.post("/", createCourse);
router.get("/", getAllCourse);

router.get("/:courseId", getCourseDetail);
router.put("/:courseId", updateCourseByID);
router.delete("/:courseId", deleteCourse);

// /courses/:courseId/reviews - Create Review
router.post('/:courseId/reviews', createReview);
// /courses/:courseId/reviews - Get all review of course
router.get('/:courseId/reviews', getAllReviewOfCourse);

module.exports = router;
