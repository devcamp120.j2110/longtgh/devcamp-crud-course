// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo Course Schema MongoDB
const courseSchema = new Schema({
    _id: Schema.Types.ObjectId, // Trường _id có kiểu dữ liệu ObjectID
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    // Trường reviews là một mảng ObjectID, mỗi một ID sẽ liên kết tới 1 review có _id tương ứng
    reviews: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Review'
        }
    ]

})

//Tạo Course Model
const CourseModel = mongoose.model("Course", courseSchema);

//Export Course Model
module.exports = { CourseModel };
