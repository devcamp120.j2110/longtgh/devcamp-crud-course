// Import thư viện mongoose
const mongoose = require('mongoose');

// Phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo Review Schema MongoDB 
const reviewSchema = new Schema({
    // Trường _id có kiểu dữ liệu ObjectID
    _id: Schema.Types.ObjectId,
    // Trường stars có kiểu dữ liệu Number, bắt buộc có
    stars: {
        type: Number,
        required: true
    },
    // Trường note có kiểu dữ liệu String, không bắt buộc
    note: {
        type: String,
        required: false
    }
});

// Tạo Model cho MongoDB từ Schema vừa khai báo
const ReviewModel = mongoose.model('Review', reviewSchema);

// Export model vừa tạo dưới dạng module
module.exports = { ReviewModel };
